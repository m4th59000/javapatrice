package com.example.javaPatrice.model;


import javax.persistence.*;
import java.util.Set;

@Entity
public class Recette {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private String description;
    private int tempsPrep;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getTempsPrep() {
        return tempsPrep;
    }

    public void setTempsPrep(int tempsPrep) {
        this.tempsPrep = tempsPrep;
    }

    public int getTempsCuisson() {
        return tempsCuisson;
    }

    public void setTempsCuisson(int tempsCuisson) {
        this.tempsCuisson = tempsCuisson;
    }

    public int getNbrPersonnes() {
        return nbrPersonnes;
    }

    public void setNbrPersonnes(int nbrPersonnes) {
        this.nbrPersonnes = nbrPersonnes;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public Difficulte getDifficulte() {
        return difficulte;
    }

    public void setDifficulte(Difficulte difficulte) {
        this.difficulte = difficulte;
    }

    public Byte[] getImage() {
        return image;
    }

    public void setImage(Byte[] image) {
        this.image = image;
    }

    public Commentaire getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(Commentaire commentaire) {
        this.commentaire = commentaire;
    }

    public Set<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(Set<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public Set<Categorie> getCategories() {
        return categories;
    }

    public void setCategories(Set<Categorie> categories) {
        this.categories = categories;
    }

    private int tempsCuisson;
    private int nbrPersonnes;
    private String source;
    private String url;

    @Lob
    private String instructions;

    @Enumerated(EnumType.STRING)
    private Difficulte difficulte;

    @Lob
    private Byte[] image;

    @OneToOne(cascade = CascadeType.ALL)
    private Commentaire commentaire;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "recette")

    private Set<Ingredient> ingredients;

    @ManyToMany
    @JoinTable( name = "recette_categorie",
            joinColumns = @JoinColumn(name = "id_recette"),
            inverseJoinColumns = @JoinColumn(name = "id_categorie"))
    private Set<Categorie> categories;
}
