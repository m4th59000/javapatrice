package com.example.javaPatrice.model;

import javax.persistence.*;

@Entity
public class Commentaire {
    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Recette getRecette() {
        return recette;
    }

    public void setRecette(Recette recette) {
        this.recette = recette;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String commentaire;

    @OneToOne
    private Recette recette;

}
