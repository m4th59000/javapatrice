package com.example.javaPatrice.model;

import javax.persistence.*;

@Entity
public class Ingredient {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private String nom;
    private Double quantite;

    @OneToOne
    private UniteDeMesure uniteDeMesure;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Double getQuantite() {
        return quantite;
    }

    public void setQuantite(Double quantite) {
        this.quantite = quantite;
    }

    public UniteDeMesure getUniteDeMesure() {
        return uniteDeMesure;
    }

    public void setUniteDeMesure(UniteDeMesure uniteDeMesure) {
        this.uniteDeMesure = uniteDeMesure;
    }

    public Recette getRecette() {
        return recette;
    }

    public void setRecette(Recette recette) {
        this.recette = recette;
    }

    @ManyToOne
    private Recette recette;


}
