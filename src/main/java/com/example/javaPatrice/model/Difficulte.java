package com.example.javaPatrice.model;

public enum Difficulte {
    FACILE, MOYENNE, DIFFICILE
}
