package com.example.javaPatrice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaPatriceApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaPatriceApplication.class, args);
	}

}
